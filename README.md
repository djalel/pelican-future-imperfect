# Future Imperfect - Pelican theme

A modern, elegant, and full of features theme for the [Pelican](getpelican.com) static blog engine. The HTML template is taken from the awesome [HTML5 UP!](https://html5up.net/) and some of its features are inspired by the [Elegant](https://github.com/talha131/pelican-elegant) theme. This theme was purposly designed to render jupyter notebooks (and maths posts in general) properly.

## Main features

- a functional search bar
- google analytics
- disqus comments
- reveal.js slides integration
- jupyter notebook integration
- mobile and desktop layout
- Progress bar for articles that corresponds to the content length without the comments

## Theme configuration

| Variable | Description | Example    |
|----------|-------------|------------|
| ABOUT | Short description to appear in the left sidebar | "Hey there!"   |
| MAIN\_LOGO | The main logo on the frontpage | images/main_logo.png |
| SHOW\_SUMMARIES | Whether to show the entire articles in the main page or only summaries | True |
| SOCIAL | Social buttons. It must be a list of couples |  (('github', 'https://github.com/DjalelBBZ'), ('email',  you@mail.io'), ('twitter', 'http://twitter.com/DjalelBBZ'),  ('linkedin', 'http://fr.linkedin.com/in/DjalelBBZ'),)|
| AVATARS | The avatars of the different authors. It must come as a dictionary |  \{ 'Djalel': 'images/avatars/djabbz.png'\}
| SOCIAL\_SIDE | Social buttons on the right sidebar |  (('github', 'https://github.com/DjalelBBZ'), ('email', you@mail.io'), ('twitter', 'http://twitter.com/DjalelBBZ'),  ('linkedin', 'http://fr.linkedin.com/in/DjalelBBZ'))|
| DISQUS\_SITENAME | Your disqus ID | 'qawarithm' |
| GA\_ACCOUNT | Your Google Analytics ID |  'UA-****' |
| POPULAR\_POSTS | The number of popular posts to display on the front page. Set to 0 to disable |  3 |
| LAST\_POSTS | The number of last posts to display on the front page. Set to 0 to disable | 3 |
| SEARCH\_RESULTS\_NUM | The number of search results to display |  3 |
| LINKS | Additional links to put in the menu bar |  [('Stuff', 'link to stuff')] |

## New settings

- EXTRA_COPYRIGHT
- NO_RIGHT_SIDEBAR

- WELCOME_MESSAGE
- NO_CATEGORIES_MENU
- NO_PAGES_MENU
- NO_MENU_BAR
- FEATURED_IMAGE_IN_POST
- WELCOME_MESSAGE
- NORIGHTSIDEBAR
- RIGHT_SIDEBAR_TITLE
- TOC_IN_MENU 
- NEXT_ARTICLE_IN_CATEGORY 
- BACKGROUND_COLOR
- SOCIAL_BUTTONS
- FILTERED_CATEGORIES_IN_INDEX
- FILTERED_CATEGORIES_IN_NEIGHBOUR_PAGES
- FILTERED_CATEGORIES_IN_MENU
- SIDEBAR_NEWS  # dict of form {'num': int, 'category', str}

## Misc
- meta accept `type: trailer`

## Necessary plugins

```
PLUGINS = ['liquid_tags.img', 'liquid_tags.video',
           'liquid_tags.youtube', 'liquid_tags.vimeo',
           'liquid_tags.include_code', 'liquid_tags.notebook',
           'extract_toc', 'related_posts', 'neighbors', 'representative_image', 'summary', 'clean_summary', 'tipue_search',  'render_math', 'autopages', 'category_order', 'pelican-cite'
           ]
```

## General recommended settings
```
STATIC_PATHS = ['images', 'pdfs', 'figures', 'downloads', 'favicon.ico']
CODE_DIR = 'downloads/code'
NOTEBOOK_DIR = 'downloads/notebooks'

THEME = 'themes/future-imperfect'

TYPOGRIFY = True
MD_EXTENSIONS = ['codehilite(css_class=highlight)', 'extra', 'headerid', 'toc']

DIRECT_TEMPLATES = ('index', 'categories', 'authors', 'archives', 'sitemap', 'robots', 'humans', 'search', '404')
ROBOTS_SAVE_AS = 'robots.txt'
HUMANS_SAVE_AS = 'humans.txt'
SITEMAP_SAVE_AS = 'sitemap.xml'
DATE_FORMATS = { 'en': '%B %d, %Y', }
DEFAULT_LANG = 'en'

SUMMARY_USE_FIRST_PARAGRAPH = True  # for summary plugin
```

## Reveal.js integration
Add `Type: slides` in the header of your markdown source

## Screenshots

## Demo


## Favicon 

Use iconified and put the result in images/iconified

## Category descriptions

Dependency: autopages plugin  
Use the subtitle meta data of the page